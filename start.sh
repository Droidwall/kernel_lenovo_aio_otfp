#!/bin/sh
echo ""
tput setaf 1;
tput bold;
echo "  _________________________ _____________________"
echo " /   _____/\__    ___/  _  \\______   \__    ___/"
echo " \_____  \   |    | /  /_\  \|       _/ |    |   "
echo " /        \  |    |/    |    \    |   \ |    |   "
echo "/_______  /  |____|\____|__  /____|_  / |____|   "
echo "        \/                 \/       \/           "

echo "PixlernBlitz Kernel By Ninad Patil Powered By SpeedHorn "
PATH=${PATH}:~/toolchains/UBERTC/aarch64-linux-android-5.3-kernel/bin/
export KBUILD_BUILD_USER="Ninad"
export KBUILD_BUILD_HOST="speedhorn"
PATH=${PATH}:~/toolchains/UBERTC/aarch64-linux-android-5.3-kernel/bin/
export ARCH=arm64 make aio_otfp_n_defconfig ARCH=arm64
CROSS_COMPILE=aarch64-linux-android- make -j5 ARCH=arm64 CROSS_COMPILE=aarch64-linux-android-

#echo " ______ _______   __ _      ___________ _   _  ______ _     _____ ___________ "
#echo " | ___ \_   _\ \ / /| |    |  ___| ___ \ \ | | | ___ \ |   |_   _|_   _|___  /"
#echo " | |_/ / | |  \ V / | |    | |__ | |_/ /  \| | | |_/ / |     | |   | |    / / "
#echo " |  __/  | |  /   \ | |    |  __||    /|     | | ___ \ |     | |   | |   / /  "
#echo " | |    _| |_/ /^\ \| |____| |___| |\ \| |\  | | |_/ / |_____| |_  | | ./ /___"
#echo " \_|    \___/\/   \/\_____/\____/\_| \_\_| \_/ \____/\_____/\___/  \_/ \_____/"
#echo "                                                                             "
#echo "                                                                             "

echo ""
tput setaf 2;
tput bold
echo " (    (        )  (          (        )         (     (               )  "
echo " )\ ) )\ )  ( /(  )\ )       )\ )  ( /(     (   )\ )  )\ )  *   )  ( /(  "
echo "(()/((()/(  )\())(()/(  (   (()/(  )\())  ( )\ (()/( (()/(  )  /(  )\()) "
echo " /(_))/(_))((_)\  /(_)) )\   /(_))((_)\   )((_) /(_)) /(_))( )(_))((_)\  "
echo "(_)) (_))  __((_)(_))  ((_) (_))   _((_) ((_)_ (_))  (_)) (_(_())  _((_) "
echo "| _ \|_ _| \ \/ /| |   | __|| _ \ | \| |  | _ )| |   |_ _||_   _| |_  /  "
echo "|  _/ | |   >  < | |__ | _| |   / |    |  | _ \| |__  | |   | |    / /   "
echo "|_|  |___| /_/\_\|____||___||_|_\ |_|\_|  |___/|____||___|  |_|   /___|  "
echo "                                                                         "
echo ""
